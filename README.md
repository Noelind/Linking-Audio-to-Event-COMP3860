# AudioTweak

AudioTweak is a 'Max for Live' application that allows the user to trigger
events that they themselves have defined. Events could range from modulating
parameter values on software instruments, launching pieces of music assigned to clips or
even send MIDI (Musical Instrument Digital Interface) notes to outboard gear such as
lights or video consoles. With AudioTweak, one does not have to rely on pre-recorded and 
time locked automations. AudioTweaks reacts to audio in real-time, allowing for free, unrestricted
expression.

### Prerequisites

Ableton Live 9.x Suite or Live 10.x Suite
Max For Live

### Running & Extending

The Max Patch can be further extended through Max's own editor once launched inside Ableton Live.

* [Ableton Live](https://www.ableton.com/en/live/) 
* [Max for Live](https://www.ableton.com/en/live/max-for-live/) 

## Authors

* **Panagiotis Penloglou** -  [Noelind](https://gitlab.com/Noelind)
