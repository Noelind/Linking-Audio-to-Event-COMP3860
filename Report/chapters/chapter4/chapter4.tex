\chapter{Development}
\label{sec:CH4}
\section{Introduction}
Entering the development stage of the project, Iterations 1 and 2 are explored in depth. Split in two individual sections, iterations are approached in a timeline fashion with each significant feature analysed. Several topics are discussed including UI and UX importance, flow of information in \textit{Max} and development challenges that arose.

Before moving to the next section, an important distinction needs to be made. \textit{Max} uses three general types of data represented by unique visual cords:
\begin{itemize}
  \item \textbf{MAX data} - Generally deals with numbers, strings and MIDI information. \textit{MAX} cords have been set to either white or yellow colour for this plugin. Different colours have been set in order to organize the patch better. White signifies normal usage whereas yellow indicates user interface functionality.
  \item \textbf{MSP data} - \textit{MSP} stands for Max Signal Processing and deals with audio signals. \textit{MSP} cords are identified by their dashed-line appearance. A legend is provided within \textit{AudioTweak} explaining the different colours used for \textit{MSP} cords. All \textit{MSP} objects have a \underline{\textbf{tilde symbol `$\sim$'}} at the end of their names to differentiate from standard \textit{MAX} objects.
  \item \textbf{Jitter data} - \textit{Jitter} is a video effects library included with \textit{Max}. Confusingly it also uses dashed-line cords but with an added black stroke. \textit{Jitter} objects were not needed for the development of the plugin.
\end{itemize}
The plugin was named \textit{AudioTweak} during development and will be referred to as such in the remaining chapters.
\section{Iteration 1}
\subsection{User Interface Design}
Prior to building \textit{AudioTweak} in \textit{Max for Live}, it was important to sketch ideas for the user interface so as to visualise the final look and feel. Prototyping the interface early on allowed for experimentation with object positioning. It is important to note that within \textit{Ableton Live} there are spatial restrictions for loaded devices. Horizontally, there is technically no restriction, however a device can only have so much width before it becomes convoluted or unnecessary. Height-wise a device has 169 pixels available; any objects placed above or below this border will not be visible in the loaded device.

One aim during this phase was to determine if all the minimum requirements and extensions (that require a visual presence) would fit in the space available for a \textit{Live} device. As an alternate option, \textit{Max} offers a mechanism that displays a floating window of a device instead of being locked in the device chain of a track. Nonetheless this method was not required in the end. \textit{AudioTweak} is presented as a standard \textit{Max for Live} device. Figure \ref{fig:CH4UI} illustrates the final (for this research project) interface for \textit{AudioTweak}. A breakdown of each element is presented in Table \ref{tab:CH4UI}.
\begin{figure}[!ht]
  %\hspace{-0.8cm}
  \fbox{\includegraphics[width=\textwidth]{./pictures/FinalUI}}
  \caption{AudioTweak user interface}
  \label{fig:CH4UI}
\end{figure}
%\vspace{-10pt}
\input{chapters/chapter4/UITable}

There have been different versions of the interface as development progressed. Figure \ref{fig:CH4UI} represents a design that came towards the end of Iteration 2. The interface was slowly built with each element added once the logic behind it was ready. In retrospect, it would have been beneficial to design the final interface with placeholder objects even if the logic was not present yet. This led to having to make continuous visual changes to the UI as development progressed. Sketched prototypes (appendix) were considered when starting out. However, when actively developing, new insight influences the interface.

In terms of positioning, it was decided that important elements should always be visible to the user. Choosing a parameter to modulate is the core activity of \textit{AudioTweak} and thus was placed first in the UI. Subsequently, fine tuning the modulation behaviour with the \textit{Attack} and \textit{Release} dials is also key, hence being positioned next to the \textit{Parameter Finder} menus. Optional features are stored in panels that can be expanded/collapsed. These features also include a power switch represented by the \href{https://www.iso.org/obp/ui#iec:grs:60417:5010}{power symbol}, a standard symbol found throughout \textit{Live}.

It is important to note that all UI behaviour must be implemented by the developer as \textit{Max} does very little to assist with this process. For example, when expanding or collapsing a panel, all elements need to be redrawn to their new positions manually. When a power switch is pressed, all related elements have to have an appropriate colour palette applied that represents their current status (activated or deactivated).

\textit{Max} used to exist before its integration with \textit{Ableton Live} and thus has its own library of interactive objects (buttons, text entry boxes, dials). These are still available to the \textit{Max for Live} developer if they wish to use them. However, when \textit{Max for Live} was released, a new set of objects became available that resemble \textit{Ableton Live} ones. These can be summoned by typing \liveobj{}, replacing \textit{objName} with the desired element. 
\begin{figure}[!ht]
  \centering
  \fbox{\includegraphics[width=0.5\textwidth]{./pictures/UIMidi}}
  \caption{Live functionality (right-click on element)}
  \label{fig:CH4LiveFunc}
\end{figure}
\pagebreak

Using these objects achieves a consistent look throughout \textit{Live}, further emphasising that \textit{Max for Live} devices are as functional as native \textit{Live} ones. Additionally, using \liveobj{} objects includes functionality that one may find with standard \textit{Live} elements such as automation drawing and MIDI mapping (Figure \ref{fig:CH4LiveFunc}).

UI elements that display numbers usually have their values changed by interacting with them (e.g. twisting a dial or dragging the mouse inside a number box). However, for more fine tuning, \textit{Live} allows these elements to have values typed in through the keyboard. This is achieved by left clicking on the element to focus it and then proceeding to type the desired value.
\subsection{First Components}
To begin developing with \textit{Max for Live}, one must create a \textit{Live} session and instantiate an empty \textit{Max} patcher. Just like built-in effects or third party plugins, \textit{Max} devices can be summoned in any \textit{Live} session on any track the user wants; they are not session dependent.
\subsubsection{Routing Audio}
Audio within \textit{Live} travels in one direction as depicted in Figure \ref{math:CH4AudioJourney}.
\begin{figure}[ht!]
  \centering
    \fbox{$ \mbox{Audio Clip} \rightarrow \mbox{Devices Chain} \rightarrow \mbox{Track Fader} \rightarrow \mbox{Master Fader}$}
  \caption{Audio journey in Ableton Live}
  \label{math:CH4AudioJourney}
\end{figure}

With regards to Figure \ref{math:CH4AudioJourney}, \textit{AudioTweak} would hypothetically be placed inside the \textit{Devices Chain} when loaded.

The next step involves routing audio into and out of \textit{AudioTweak}. This is achieved by using the \href{https://docs.cycling74.com/max7/maxobject/plugin~}{\plugin} and \href{https://docs.cycling74.com/max7/maxobject/plugout~}{\plugout} objects inside the \textit{Max} editor. They act as entry and exit points for audio, using \textit{MSP} cords to dictate the flow of the audio signal.

At this point, it is important to address how these objects are used in \textit{AudioTweak} and how their use differs from other devices. Most plugins aim to process audio either to enhance it sonically or to create interesting musical ideas. These changes are made apparent when the user starts playing around with the options available inside a plugin.
\pagebreak

\noindent
To visualise\footnote{Heavy arrow: Device entry or exit point. Normal arrow: signal flow within device.}:
\begin{figure}[h]
  %\begin{mdframed}[linewidth=0.5mm]
  \fbox{$ \mbox{Audio from track} \Rightarrow \plugin \rightarrow SomeSignalProcess() \rightarrow \plugout   \Rightarrow \mbox{Audio to track}$}
  %\end{mdframed}
  \caption{Example plugin routing}
  \label{math:CH4PEPRouting}
\end{figure}
\\
\textit{AudioTweak}'s purpose is to use fed audio to modulate a user-selected parameter. The user does not expect the incoming audio to be altered in any way; the only change they are expecting are the values to be modulated for the selected parameter. The audio signal reaching the parameter is not the same signal reaching the user. The differences between these two signals are:
\begin{itemize}
  \item Modulating signal is gain-staged (optional component).
  \item Modulating signal is summed to mono.
  \item Modulating signal is filtered (optional component).
\end{itemize}
\hfill\break
To visualise:
\begin{mdframed}[linewidth=0.5mm]
  $ \mbox{Audio from track} \Rightarrow \plugin\rightarrow \plugout\Rightarrow \mbox{Audio to track}$ \\
  $\hspace*{1em} \Downarrow$ \\
  $ \plugin\rightarrow GainStage(optional) \rightarrow SumToMono() \rightarrow Filter(optional) \rightarrow parameter$
\end{mdframed}
\begin{figure}[ht!]
  \caption{AudioTweak routing}
  \label{math:CH4ATRouting}
\end{figure}
It is possible to have more than one \plugin{} objects inside the patch. Every \plugin{} creates a copy of the incoming audio signal. In this case, there are 2 copies of the signal travelling along different chains. The first copy gets routed right back to \plugout{} without being altered at all. There is no audible difference to the user with this chain. The second copy is routed through a chain of processes leading to the modulation of the selected parameter. These changes are not heard but are visualised as movement (or modulation) of the parameter. If the user wants to listen to the signal from the bottom chain, he must hold down the \textit{Preview} button as seen in Figure \ref{fig:CH4UI}.
\subsubsection{Gain Staging}\label{subsec:CH4GainStage}
One of the first features to be included in \textit{AudioTweak} was an amplitude slider. Specifically, \href{https://docs.cycling74.com/max7/maxobject/live.gain~}{\livegain} was designed to be a developer-friendly implementation of an amplitude slider. Through its two inlets, it accepts left and right audio signals. The needle controls the new amplitude level in decibels before it is sent out.
\\
The reason \livegain{} has been included is to control the incoming amplitude information before it reaches the parameter. Some scenarios that describe its usage:
\begin{itemize}
  \item The incoming audio has its volume correctly set for the musical context to which it belongs. There is a possibility that it is too quiet or too loud when it reaches the parameter, producing undesirable results in the modulation.
  \item Since \livegain{} is a \liveobj{} object, its UI can be mapped to a MIDI controller. The performer can control the intensity of the modulation through \livegain{} without affecting the audible signal.
\end{itemize}
This feature is optional within \textit{AudioTweak} and can be bypassed through a power switch. Figure \ref{fig:CH4LiveGain} demonstrates how \livegain{} has been applied:
\begin{figure}[ht!]
  \begin{center}
    \fbox{\includegraphics[scale=0.25]{./pictures/GainStage}}
    \caption{\livegain{} within AudioTweak}
    \label{fig:CH4LiveGain}
  \end{center}
\end{figure}
\pagebreak
\subsubsection{Summing to Mono}\label{subsec:CH4Sum2Mono}
\href{https://en.wikipedia.org/wiki/Monaural}{\textit{Monophonic}} or \textit{Mono} audio describes sounds that emanate from a single position in space. Summing to Mono is the process of reducing a stereophonic signal (sounds with dimension) into a monophonic signal. In \textit{Max}, a stereo signal is depicted by two \textit{MSP} cords as seen in Figure \ref{fig:CH4LiveGain} (e.g. left and right cords from the \plugin{} object). Note that the colours on \textit{MSP} cords do not alter the signal in any way. Colours can be chosen by the developer for better patch organisation.

In this case the red cords are activated when \livegain{} is turned off, therefore immediately summing the original stereo signal to mono. Oppositely, when \livegain{} is turned on, the blue cords are activated. Before the stereo signal is summed to mono it is scaled by \livegain.

Within \textit{AudioTweak}, there is no need for a stereo signal to modulate the parameter. It would be unnecessarily complicated to implement this for no real benefit.

In order to sum to mono, both signals from a stereo pair are routed through a \pollaplasiasmos{} object (referred to as a \textit{multiply} object) and multiplied by $0.5$. This can be seen at the very bottom of Figure \ref{fig:CH4LiveGain}.

To visualise:
\begin{figure}[ht!]
\begin{center}
  \fbox{$ (LeftSignal * 0.5) + (RightSignal * 0.5)  = MonoSignal$}
\end{center}
\caption{Sum to Mono equation.}
\label{math:sum2mono}.
\end{figure}

Each signal from the stereo pair has its own amplitude level. Without multiplying by $0.5$, the addition of these signals would lead to \href{https://en.wikipedia.org/wiki/Clipping_(audio)}{clipping}, a form of digital distortion.
\subsection{Targeting Parameters}
\subsubsection{Searching for Parameters}
In order for \textit{Max} to have access over \textit{Live} objects, Ableton have made \textit{LOM} available to \textit{Max for Live} users. The \textit{LOM} depicts \textit{Live} object classes alongside their properties and functions, as well as parent-child relations within a hierarchical diagram. For a device developer, it is meant to be used as map illustrating paths to desired elements.

One of the requirements for this device is being able to choose a device parameter through a specific chain of menus:
%\begin{mdframed}[linewidth=0.5mm]
\begin{figure}[ht!]
\begin{center}
  \fbox{$Track \rightarrow Devices \rightarrow Parameter$}
\end{center}
\caption{Parameter Finder path}
\end{figure}
%\end{mdframed}

In the case of \textit{r-chain III} (Subsection \ref{subsec:r-chainIII}), the starting point for this search was \textit{Devices} instead of \textit{Track}. The UI was limited to viewing all available devices at once with no indication as to which track they belong to. Making a selection becomes unnecessarily hard if the same device is loaded across multiple tracks.

The use of \textit{Browse.DeviceParameters.maxpat} has corrected  this issue in \textit{AudioTweak}. It is a \textit{Max} patch included in the standard library written by Cycling '74 developer Jeremy Bernstein. It is made up of three hierarchical dropdown menus that are populated based on the user's decision. It can be seen in Figure \ref{fig:CH4UI} on the far left.

One of the biggest development challenges during this iteration was extending the functionality of \textit{Browse.DeviceParameters.maxpat}. When a dropdown menu is clicked, all currently available elements appear in the menu (e.g. all tracks within a session appear when the \textit{Track} menu is selected). When elements are added or deleted, the dropdown menus would reflect this and be correctly updated. One feature missing was the ability to listen to \textit{Track} or \textit{Device} rename events.

This has been partially corrected within AudioTweak. \textit{Browse.DeviceParameters.maxpat} is actually made up of three copies of \textit{M4L.chooser.maxpat}. This patch contains a placeholder dropdown menu but is not predefined to search for a specific category. Instead, \textit{M4L.chooser.maxpat} accepts a string argument that dictates which category to look for when summoned.

To incorporate a viewable patch within a higher patch the \href{https://docs.cycling74.com/max7/maxobject/bpatcher}{\textit{bpatcher}} object must be used \parencite{Manzo}. In order to preserve the original version of the file, I made a copy named \textit{M4L.chooserSC14PP.maxpat}. Next I implemented a \href{https://docs.cycling74.com/max7/vignettes/encapsulate_deencapsulate}{\textit{subpatch}} within \textit{M4L.chooserSC14PP.maxpat} that listens for rename events. Figure \ref{fig:CH4ChooserRename} showcases these changes.
\newpage
\begin{figure}[ht!]
  \centering
  \fbox{\includegraphics[width=\textwidth]{./pictures/ChooserRename}}
  \caption{Renaming logic subpatch within \textit{M4L.ChooserSC14PP.maxpat}}
  \label{fig:CH4ChooserRename}
\end{figure}

\subsubsection{Modulating parameters} \label{subsubsec:CH4ManPar}
Every object in \textit{Live} is assigned a unique ID number regardless of the type of class (e.g. tracks, clips, devices or parameters). Using the \textit{LOM} as a guide, the developer can produce these ID numbers to target \textit{Live} objects. To perform operations on the desired object, a \href{https://docs.cycling74.com/max7/maxobject/live.remote~}{\liveremote{}} object must be used. With two inlets equipped, the left one accepts float numbers or an audio signal and the right one accepts ID numbers of \textit{Live} objects.

Once supplied with an ID number, \liveremote{} assumes control over the parameter. Its values cannot be altered by the user as long as this connection remains alive. Automation of the parameter is also disabled since the source of new values would be ambiguous (automation lines or instructions from \liveremote). When this connection is established, a message appears in \textit{Live}:
\begin{figure}[ht!]
  \begin{center}
    \fbox{\includegraphics[scale=1.1]{./pictures/MasterSlave}}
    \caption{Master-Slave relationship between \liveremote{} and parameter}
    \label{fig:CH4MasterSlave}
  \end{center}
\end{figure}

As mentioned above, the left inlet of \liveremote{} accepts float numbers or audio signals which in turn will produce new values for the controlled parameter. In \textit{Iteration 1}, the Mono signal previously produced was routed to the left inlet of \liveremote{}. A key moment during development, this was when a parameter began to be affected by user selected audio.

Unfortunately the results produced were not optimal. There were two problems in particular that needed to be addressed:
\begin{enumerate}
  \item Due to the constant stream of the Mono signal, the parameter had very abrupt changes and erratic behaviour. The connection between music and parameter felt chaotic.
  \item There was no clear understanding that the values being sent to the parameter were meaningful. At this stage it was not known what information the \textit{MSP} cord was consisted of. Additionally, what values would make sense to a selected parameter was unknown.
\end{enumerate}

\section{Iteration 2}
\subsection{Improving modulating algorithm}
The first problem that was addressed in \textit{Iteration 2} was the issue discovered at the end of \textit{Iteration 1}. The combination of objects affecting the parameter were not optimal and had to be improved.
\subsubsection{The \adsr{} Object}
In music technology terminology, an \href{https://musictechstudent.co.uk/music-production-glossary/adsr/}{envelope} describes a waveform curve, most commonly associated with the ADSR (Attack-Decay-Sustain-Release) paradigm. The ADSR describes four distinct stages of life for a waveform; specifically the amplitude of the wave over the period of each stage. After further research, it was decided that an ADSR component would resolve the abruptness issue mentioned in subsection \ref{subsubsec:CH4ManPar}.

As part of its \textit{MSP} library, \textit{Max} has an envelope generator called \href{https://docs.cycling74.com/max7/maxobject/adsr~}{\adsr{}}. This object has 5 inlets. The leftmost inlet accepts float numbers or audio signals whilst the subsequent inlets accept values describing an ADSR envelope (e.g. inlet 2 describes the attack of the generated envelope).

Routing the Mono signal through \adsr{} with some test values for the attack and release led to a much more stable interaction between audio and parameter. For this reason it was decided to include two new UI elements controlling the attack and release values for the \adsr{} object. They can been seen above the \textit{Target Range} component in Figure \ref{fig:CH4UI}.

The buttons above the dials have two interchangeable modes, \textit{Short} and \textit{Long}. When set to \textit{Short}, the dial has a range 0 to 1000 milliseconds. When set to \textit{Long}, the dial has a range of 0 to 20 seconds. This option was included so that the user has a large spectrum of options when designing the modulating behaviour.

A good value for the attack dial can generally be found between 100 and 500 ms. This produces an instant yet almost calm reaction in the parameter's behaviour when being modulated by music. The source audio is of extreme importance when determining the modulation behaviour and values that work in one context might not work for another.
\subsubsection{Parameter Value Range}
The \href{https://docs.cycling74.com/max7/maxobject/meter~}{\meter{}} object (Figure \ref{fig:CH4Meters}) is a visual peak level indicator. It accepts audio signals and outputs the peak value of those signals between developer-defined intervals. Peak value refers to the loudness of a signal for the specific sample that was analysed during an interval. The output is a float number and is represented by a regular \textit{MAX} cord, not an \textit{MSP} cord. The output of \meter{} ranges from $[0.0,\infty]$ with $0.0$ indicating complete silence and all values up to and including $1.0$ indicating healthy volume levels. Any value over $1.0$ indicates overload with the UI of \meter{} turning red.
\begin{figure}[ht!]
  \begin{center}
    \fbox{\includegraphics[scale=0.8]{./pictures/meter}}
    \caption{Example of two \meter{} objects}
    \label{fig:CH4Meters}
  \end{center}
\end{figure}

By routing the Mono signal through the \meter{} object, I could begin to determine the type of values available to me before sending them to the parameter. During development, it was concluded that the \adsr{} object is expecting signal values in the range $[0,100]$. Therefore a translation is required for these two objects to understand each other.

The \href{https://docs.cycling74.com/max7/maxobject/scale}{\scale{}} object can map any incoming number into a corresponding number of a desired range. Figure \ref{math:CH4scale} demonstrates how \scale{} works.
\begin{figure}[ht!]
\begin{mdframed}[linewidth=0.5mm]
  \begin{center}
    $\mbox{Input number} \rightarrow scale([inputRange],[outputRange]) \rightarrow \mbox{Output number}$ \\
    \vspace{2mm}
    $0.5 \rightarrow scale([0.0,1.0],[0,100]) \rightarrow 50$
  \end{center}
\end{mdframed}
\caption{\scale{} object example}
\label{math:CH4scale}
\end{figure}
\hfill \break
Because \adsr{} outputs an MSP signal, this has to be converted back to float numbers. This is achieved with the \href{https://docs.cycling74.com/max7/maxobject/snapshot~}{\snapshot{}} object. It is very similar to \meter{} in that it can accept audio signals and output peak value of `snapshotted' intervals. The difference is that it does not include a visual meter, therefore using less resources.

The next step involves a sibling object of \scale{} called \href{https://docs.cycling74.com/max7/maxobject/zmap}{\zmap{}}. They both perform the same function, mapping numbers from an input range to an output range. The difference is that \scale{} uses a soft clip on the newly mapped numbers, meaning that numbers under or over the output range may be produced. \zmap{} uses a strict clip that respects the given output range.

Finally, the \zmap{} object is connected to the left inlet of \liveremote{} and begins to produce new values for the parameter. Through \zmap, it is also possible to customize the output range so that the output becomes meaningful to the selected parameter. For example, if a parameter has a minimum value of $-6$ and a maximum value of $3$ then \zmap{} would be set as such:
\begin{figure}[ht!]
\begin{mdframed}[linewidth=0.6mm]
  \begin{center}
    $\mbox{Mono Signal as float} \rightarrow zmap([0.0,1.0],[-6.0,3.0]) \rightarrow \mbox{Newly mapped number}$
  \end{center}
\end{mdframed}
\caption{\zmap{} example usage}
\end{figure}

Since this component is crucial to \textit{AudioTweak}'s operation, two number boxes were added to the UI that control the output range of \zmap. Labeled \textit{Target Range}, they can be seen under the \textit{Attack} and \textit{Release} dials in Figure \ref{fig:CH4UI}. Table \ref{tab:CH4zmap} contains common parameter ranges that were found whilst testing various devices with \textit{Live} (not exhaustive):
\pagebreak
\input{chapters/chapter4/zmapTable}
\\
Optionally, a subrange can be fed into \zmap. For example if the range $[0.5,1.0]$ is used, then the parameter will always rest at 0.5 during silence.
Figure \ref{fig:CH4ModAlg} demonstrates how the objects described in this subsection are connected:
% The next figure illustrates the use of these objects and how they have been connected within Max: 
\begin{figure}[ht!]
  %\hspace{-0.2cm}
  \centering
  \fbox{\includegraphics[width=0.7\textwidth]{./pictures/adsr}}
  \caption{Final modulating algorithm}
  \label{fig:CH4ModAlg}
\end{figure}
\subsection{Filter Mode}
%cite https://docs.cycling74.com/max7/tutorials/08_filterchapter01
In music technology, a \href{https://www.soundonsound.com/sound-advice/q-what-are-filters-and-what-do-they-do}{\textit{filter}} (either hardware or software) is designed to alter the shape of an audio signal. Specifically, a filter has control over the frequency spectrum. The user can select a frequency range (known as \textit{band}) and proceed to either increase or decrease the amplitude of that range. This process can be used to produce interesting musical results or to correct problems present in the signal (e.g. hiss in the high frequencies).

One of the minimum requirements for this project was to implement a filter inside \textit{AudioTweak}. Majority of \textit{Iteration 2} was spent developing this feature.

\textit{Max} offers an abstraction of a filter within its standard library. The \href{https://docs.cycling74.com/max7/maxobject/filtergraph~}{\filtergraph{}} object is primarily a visual component that emulates the behaviour of digital parametric equalizers. Its interface is controlled by clicking and dragging the mouse to move the bands. Movement on the $x$-axis determines the frequency range of the band. Movement on the $y$-axis determines the amplitude of the currently selected range.

Note that \filtergraph{} takes no part in actually altering the signal. To alter the signal a \href{https://docs.cycling74.com/max7/maxobject/biquad~}{\biquad{}} object must be used. \biquad{} has 6 inlets and a single outlet. Provided with an \textit{MSP} signal, \biquad{} will output a filtered version of that signal based on 5 given coefficients (the remaining inlets). The equation used to calculate the filtered signal is:
\begin{figure}[ht!]
\begin{mdframed}[linewidth=0.5mm]
  \begin{center}
    $y[n] = \textbf{a0} * x[n] + \textbf{a1} * x[n-1] + \textbf{a2} * x[n-2] - \textbf{b1} * y[n-1] - \textbf{b2} * y[n-2]$
  \end{center}
\end{mdframed}
\caption{Filtering equation}
\label{math:CH4FilterEquation}
\end{figure}

The coefficients in bold are generated by \filtergraph{} and subsequently sent to \biquad{} as an array.

The \filtergraph{} object required considerable tweaking in terms of user interactivity. Firstly the maximum gain value the user could draw was extremely high, to the point where digital clipping could harm someone's hearing. Secondly, the movement speed on both the $x$ and $y$ axis when using the mouse was also too high. Both of these issues were immediately addressed.

A common design pattern found in parametric equalizers are  three assistant dials next to the frequency spectrum. These represent the frequency, the amplitude and the bandwidth values for a given band. With these dials, a user can choose how he interacts with the filter; either by manually dragging the mouse for a quick configuration or by typing specific values in the dials for more precision. A relationship exists between the spectrum component and the dials where if one is moved the other is updated and vice versa.
\input{chapters/chapter4/filterTable}

Table \ref{tab:CH4FilterTypes} details the type of filters that \filtergraph{} can implement. These are available to the user through a dropdown menu located above \filtergraph. The complete filter panel as a user interface element can be seen in Figure \ref{fig:CH4UI} to the right.

For more information on filters, a recommended resource is \textit{The computer music tutorial} \parencite{roads}.
\subsection{States and UI Logic}
With all the necessary components implemented, the state logic required updating. There are four possible states for the Mono signal:
\begin{itemize}
\item It uses no optional component.
\item It uses only the \textit{Gain} component.
\item It uses only the \textit{Filter} component.
\item It uses both the \textit{Gain} and \textit{Filter} components
\end{itemize}
Technically, \textit{Max} or \textit{MSP} objects do not have properties that would disable their activity (such as obj.enable() or obj.disable()). The only way to stop an object from running is to interrupt its input source. The \href{https://docs.cycling74.com/max7/maxobject/gate}{\gate{}} and \href{https://docs.cycling74.com/max7/maxobject/gate~}{\gateMSP{}} objects facilitate this. As the name implies, they act as gates which open and close conditionally. By establishing a flag system within \textit{AudioTweak}, a set of \gate{} objects is responsible for allowing the right Mono signal reach the parameter. An example of a flag as a string would be `\textit{filter\_off}'. This message allows the signal to go through \livegain{} but ignore the filter component entirely. This system is controlled by the power switches found on the interface.

Additionally, the user interface needed some modest adjustments to accommodate these new features. Since \textit{Max} does not include ready UI panels that expand/collapse, this logic had to be written from scratch. A subpatcher named \textit{ShowHideUI} is responsible for the UI behaviour during user interaction.

With regards to the \textit{Gain} and \textit{Filter} panels, there are four possible activity states:
\hfill \break
\hfill \break
\begin{minipage}[t]{.5\textwidth}
  \begin{itemize}
    \item Gain $=$ Collapsed - Filter $=$ Collapsed
    \item Gain $=$ Collapsed - Filter $=$ Expanded
  \end{itemize}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
  \begin{itemize}
    \item Gain $=$ Expanded - Filter $=$ Collapsed
    \item Gain $=$ Expanded - Filter $=$ Expanded
  \end{itemize}
\end{minipage}%
\\
\hfill \break
\hfill \break
Each state is controlled by a separate piece of logic. For example, the filter panel needs to be redrawn in different positions depending on the state of the gain panel. If the gain panel is expanded, the filter needs to be repositioned further to the right. Additionally, for every panel expansion, the device itself needs to be resized to accommodate the elements.

A different flag system has been implemented for UI updates. For instance when \textit{Gain} is turned on and \textit{Filter} is turned off the string `\textit{gain\_expand filter\_collapse}' is sent to the \textit{ShowHideUI} subpatch and runs the corresponding logic.