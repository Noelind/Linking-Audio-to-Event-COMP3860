\chapter{Project Structure}
\label{chapter3}

\section{Introduction}
This chapter examines how this research project was planned and how development took place. It includes a `before and after' Gantt chart that displays the project's development over time as well as an examination of development tools (API introduction and version control system).
\\
\section{Timeline} \label{ch3PlanvsDev}%3.2
\subsection{Preamble}
%% Talk about the gantt chart below. Explain that iterations are on the next page.
Following the module guide, it was decided that all background research and interim report writing would take place in Semester 1. This allowed for a proper examination of already available solutions and potential technologies. In addition, a Git repository was set up to house all work related to this project. Included in the interim report was a Gantt chart that detailed a proposed timeline for this project. This chart is revisited in this report, highlighting the differences between planned and actual development.

After the interim report was submitted and \textit{Max} had been chosen as the development platform, I familiarised myself with \textit{Max} over the Christmas break. I made sure all necessary software was installed so as to not waste time during Semester 2.

Following exam period in January, work began on the plugin starting with \textit{Iteration 1}. A detailed table of iterations is included in this chapter. Issue tracking and milestones are also examined.
\subsection{Gantt Chart Comparison}
The next two pages illustrate the proposed versus actual project evolution in the form of Gantt charts. Both charts share a similar structure but some elements differ. Time is split in phases denoted by yellow brackets. Any item under those brackets belong to that particular phase. Items in red denote actions that take place over a period of time. Important deadlines or meetings are depicted by a black diamond.
\newpage
\input{chapters/chapter3/ganttPlan}
\newpage
\input{chapters/chapter3/ganttActual}
\newpage
\noindent
Right up to the \textit{Intermediate Report} deadline, the project was on schedule. This is not surprising since all actions had to be completed in order to submit the interim report on time. One error in judgement was not anticipating the amount of revision required for January exams. This halted development slightly and \textit{Iteration 1} was pushed back by several weeks. However \textit{Iteration 1} was completed only just one week later than the proposed date. \textit{Iteration 2} introduced some unforeseen challenges that are explored in chapter \ref{sec:CH4}.

User evaluation did not take place at the proposed time due to not having found suitable candidates in time. User testing and findings are discussed in chapter \ref{sec:CH5}.
\section{Project Milestones - Iterations}\label{sec:CH3Iterations}
This project is split into 4 development iterations. \textit{Iterations 1} and \textit{2} fulfil the minimum requirements and cannot be skipped. \textit{Iterations 3} and \textit{4} fulfil the extensions and are not required to complete the project. Table \ref{tab:CH3Iter} illustrates the features developed in each iteration; each feature has a priority tag denoting how critical its status is.
\input{chapters/chapter3/iterationsTable}

\section{Risk Mitigation} \label{ch3Risk}
This project was prone to risks affecting the schedule due to its exploratory nature. Difficulties arose due to not being familiar with \textit{Max} when I started out. This is why during the Christmas holidays I spent time running small demos in order to become better acquainted with the chosen API. These experiments proved to be beneficial for the project as I realised that \textit{Max} was a well suited platform for this project. Luckily, there was never a need to abandon \textit{Max} and switch to a different API.

\section{Version Control with Git}
Early on it was decided that a VCS (version control system) needed to be in place. Housing code and report writing to a VCS removes any possibility that project progress is lost along the way. It was of great importance to able to retrieve previous versions of the plugin. There were a number of times where a development session became too convoluted and unusable; a pull operation from the last commit quickly rectified this.

Having previously used \href{https://about.gitlab.com/}{GitLab} for different modules, I decided it was a good platform to use for this project. The project is split into two different branches, namely \textit{Report} and \textit{Plugin}. The \textit{Report} branch houses all relevant Latex (\LaTeX) files required to produce this report. The \textit{Plugin} branch houses the following:
%\pagebreak
\begin{description}
  \item [AudioTweak.amxd] - The main patcher file required to run the plugin. `\textit{amxd}' stands for \textit{Ableton Max Device}.
  \item [AudioTweak.txt] - Same as above but in text format. All \textit{Max} patches are actually written in a cascade style language that are translated visually in the \textit{Max} editor.
  \item [AudioTweakFilter.maxpat] - A subpatch containing the filter component of the plugin.
  \item [M4l.chooserSC14PP.maxpat] - A \textit{Max} patch included with the built-in library but further edited by me.
  \item [M4l.chooserSC14PP.js] - Directly linked to the file above, this JavaScript file contains methods that make use of the \textit{LOM} (Live Object Model - see Appendix \ref{fig:LOM}), \textit{Ableton Live}'s API.
  \item [In-editor screenshots] - These screenshots showcase the plugin in its \textit{patching mode} as its being developed. They are primarily for the benefit of any reader who has not got access to \textit{Max} and wishes to see how the plugin was made.
\end{description}
Apart from serving as a VCS, GitLab also includes issue tracking capabilities. Features that needed to be developed as seen in the iterations table were logged as separate \textit{Issues} in GitLab. Additionally, all issues belonging to the same iteration were added to a parent \textit{Milestone}, namely \textit{Iteration 1} and \textit{Iteration 2}. Each milestone had an end date, which helped keep the project on track.

Inevitably during development, bugs arose that hindered the plugin's progress. If these bugs could not be dealt with in the current development session, they were added as issues in GitLab.
\section{Development Tools}
\subsection{Ableton Live}
\textit{Ableton Live} or simply \textit{Live} (Figure \ref{fig:CH3Ableton}) is a digital audio workstation available for macOS and Windows. It was envisioned that the plugin would work with \textit{Live} in its finished state; it is a DAW I am most familiar with, unlike other workstations.

It stands out from other competitor software both from a design and an extendibility standpoint. Most DAW software offer a single timeline view in an effort to emulate analogue music studios. \textit{Live} was made with the prospect of being used in live settings, not just as a production suite. When \textit{Max for Live} became available as an add-on, it allowed for artists to further customise their process and workflow.

At the time of writing the latest major release of \textit{Live} was version 10, released on February 6, 2018. However this research project began using \textit{Live 9}. Although upgrading to \textit{Live 10} and migrating to \textit{Max 8} should be straightforward, continuing with \textit{Live 9} seemed to be a safer and more stable choice at the time. Note that \textit{Max 8} is officially released in September 2018; \textit{Live 10} has a beta version of \textit{Max 8} built-in which could have possibly been problematic.
\begin{figure}[ht!]
   \centering
    \fbox{\includegraphics[width=\textwidth]{./pictures/Live9Example}}
  \caption{Ableton Live 9 - Arrangement View}
  \label{fig:CH3Ableton}
\end{figure}
\subsection{Developing in Max}
\textit{Max} is a visual programming language. Within the \textit{Max} editor (Figure \ref{fig:CH3MaxEditor}), a developer can create and arrange objects in a two dimensional plane. 
\begin{figure}[ht!]
  \centering
    \fbox{\includegraphics[width=0.9\textwidth]{./pictures/Max7Example}}
  \caption{Max 7 editor}
  \label{fig:CH3MaxEditor}
\end{figure}
Objects usually have a number of inlets and outlets as means of connecting to each other via patch cords. Thus the developer dictates the flow of information in an almost schematic way, similar to a circuit diagram.

Specifically for this project, a special version of \textit{Max} has been implemented called \textit{Max for Live}. This version allows \textit{Ableton Live} users to extend their DAW's capabilities whilst never having to worry about compatibility issues or ever having to leave the DAW. \textit{Max for Live} devices can be used within a \textit{Live} session as soon as they have been created. This allows for rapid testing as the developer can observe device behaviour within \textit{Live} instantly.
\textit{Max 7} was used to develop the plugin.

\subsection{Other Development Tools}
These tools were secondary in contrast to \textit{Max} but were nonetheless significant in realising this project:
\begin{itemize}
  \item \href{https://www.overleaf.com/}{\textbf{Overleaf}} - An online LaTeX and Rich Text writing tool. It compiles projects as they are being written in real-time for a more WYSIWYG LaTeX experience.
  \item \href{https://www.sublimetext.com/}{\textbf{Sublime Text 3}} - A source code editor.
  \item \textbf{Adobe} \href{https://www.adobe.com/uk/products/photoshop.html}{\textbf{Photoshop}} \& \href{https://www.adobe.com/uk/products/illustrator.html}{\textbf{Illustrator}} - Graphics editor software for either prototyping UI designs or implementing graphics within the plugin UI.
\end{itemize}



